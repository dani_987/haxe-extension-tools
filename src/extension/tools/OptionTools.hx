package extension.tools;

import haxe.ds.Option;

class OptionTools {
    @:generic
    public static function create<T>(fromValue: Null<T>) : Option<T> {
        if(fromValue == null) return Option.None;
        else return Option.Some(fromValue);
    }

    @:generic
    public static function ifPresent<T>(value: Option<T>, toCall: T -> Void) : Option<T> {
        switch(value){
            case Some(ofValue): toCall(ofValue);
            default:
        }
        return value;
    }

    @:generic
    public static inline function isPresent<T>(value: Option<T>) : Bool {
        switch(value){
            case Some(_): return true;
            default: return false;
        }
    }

    @:generic
    public static function ifNotPresent<T>(value: Option<T>, toCall: () -> Void) : Option<T> {
        switch(value){
            case Option.None: toCall();
            default:
        }
        return value;
    }

    @:generic
    public static inline function getOrThrow<T>(value: Option<T>, message: String = "Some value is required") : T {
        switch(value){
            case Option.None: throw message;
            case Some(ofValue): return ofValue;
        }
    }

    @:generic
    public static inline function getOrDefault<T>(value: Option<T>, defaultValue: T) : T {
        switch(value){
            case Option.None: return defaultValue;
            case Some(ofValue): return ofValue;
        }
    }

    @:generic
    public static function map<T,R>(value: Option<T>, mapper: T->R) : Option<R> {
        switch(value){
            case Option.None: return Option.None;
            case Some(v): return Some(mapper(v));
        }
    }

    @:generic
    public static function unwrap<T>(value: Option<Option<T>>) : Option<T> {
        switch(value){
            case Some(Some(v)): return Some(v);
            case _: return Option.None;
        }
    }
}
