package extension.tools;

import haxe.EnumTools;
import haxe.EnumTools.EnumValueTools;
using haxe.Int64;

class ReflectTools {
    public static function compare(a:Dynamic, b:Dynamic) : Int {
        if(a == b)return 0;
        if(Type.getClassName(Type.getClass(a)) != Type.getClassName(Type.getClass(b)))
            return Reflect.compare(Type.getClassName(Type.getClass(a)), Type.getClassName(Type.getClass(b)));
        if( (a is Int)
         || (a is Float)
         || (a is String)
         || (a is Bool)
        ){
            return Reflect.compare(a, b);
        }
        if( a.isInt64() ){
            return a.compare(b);
        }
        if(Reflect.isFunction(a)){
            return if (Reflect.compareMethods(a, b)) 0 else if (a > b) 1 else -1;
        }
        if(Reflect.isEnumValue(a)){
            if(EnumTools.getName(Type.getEnum(a)) != EnumTools.getName(Type.getEnum(b))){
                return Reflect.compare(EnumTools.getName(Type.getEnum(a)), EnumTools.getName(Type.getEnum(b)));
            }
            if(EnumValueTools.getName(a) != EnumValueTools.getName(b)){
                return Reflect.compare(EnumValueTools.getName(a), EnumValueTools.getName(b));
            }
            var pa = EnumValueTools.getParameters(a);
            var pb = EnumValueTools.getParameters(b);
            if(pa.length != pb.length)return Reflect.compare(pa.length, pb.length);
            for(i in 0...pa.length){
                var res = compare(pa[i], pb[i]);
                if(res != 0){
                    return res;
                }
            }
            return 0;
        }
        try{
            var aa = a.iterator();
            var ab = b.iterator();
            while(aa.hasNext() && ab.hasNext()){
                var res = compare(aa.next(), ab.next());
                if(res != 0){
                    return res;
                }
            }
            return Reflect.compare(aa.hasNext(), ab.hasNext());
        } catch(_){}
        try{
            var aa = a.keyValueIterator();
            var ab = b.keyValueIterator();
            while(aa.hasNext() && ab.hasNext()){
                var res = compare(aa.next(), ab.next());
                if(res != 0){
                    return res;
                }
            }
            return Reflect.compare(aa.hasNext(), ab.hasNext());
        } catch(_){}
        var fa = Reflect.fields(a);
        var fb = Reflect.fields(b);
        fa.sort(Reflect.compare);
        fb.sort(Reflect.compare);
        if(fa.length != fb.length)return Reflect.compare(fa.length, fb.length);
        for(i in 0...fa.length){
            if(fa[i] != fb[i])return Reflect.compare(fa[i],fb[i]);
        }
        for(fld in fa){
            var res = compare(Reflect.field(a, fld), Reflect.field(b, fld));
            if(res != 0){
                return res;
            }
        }
        return 0;
    }
}
