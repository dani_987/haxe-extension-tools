package extension.tools;


class DefaultTools {
    @:generic
    public static inline function orDefault<T>(value: Null<T>, defaultValue: T) : T {
        if(value == null) return defaultValue;
        else return value;
    }
    @:generic
    public static inline function orThrow<T>(value: Null<T>, message: String) : T {
        if(value == null) throw message;
        else return value;
    }
}
