package test;
import buddy.BuddySuite;
using buddy.Should;
using extension.tools.DefaultTools;


class DefaultToolsWorking extends BuddySuite{
    public function new() {
        var realValue = 5;
        var defaultValue = 1;
        var nullValue : Null<Int> = null;
        describe("DefaultTools", {
            it("should return the default value, if null is given",{
                nullValue.orDefault(defaultValue).should.be(defaultValue);
            });
            it("should return the input value, if not null is given",{
                realValue.orDefault(defaultValue).should.be(realValue);
            });
        });
    }
}
