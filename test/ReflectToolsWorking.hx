package test;
import buddy.BuddySuite;
import haxe.ds.Option;

using buddy.Should;
using extension.tools.ReflectTools;


class ReflectToolsWorking extends BuddySuite{
    public function new() {
        describe("ReflectTools", {
            it("compare should work",{
                2.compare(5).should.be(-1);
                5.compare(5).should.be(0);
                "5".compare(5).should.not.be(0);
                5.0.compare(5).should.be(0);
                var v = new List();
                v.add(5);
                v.compare([5]).should.not.be(0);
                [5].compare([5]).should.be(0);
                [1,2].compare([5]).should.be(-1);
                [5,1].compare([5]).should.be(1);
                var t = ()->true;
                var f = ()->false;
                t.compare(f).should.not.be(0);
                f.compare(t).should.be(t.compare(f));
                t.compare(t).should.be(0);
            });
        });
    }
}
