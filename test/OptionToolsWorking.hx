package test;
import buddy.BuddySuite;
import haxe.ds.Option;
using buddy.Should;
using extension.tools.OptionTools;


class OptionToolsWorking extends BuddySuite{
    public function new() {
        var someValue = 5;
        var optionSet = OptionTools.create(someValue);
        var optionEmpty : Option<Int> = OptionTools.create(null);
        describe("OptionTools", {
            it("if(Not)Present should work",{
                var called = 0;
                optionSet.ifPresent((value) -> {
                    value.should.be(5);
                    called++;
                }).ifNotPresent(() -> {
                    true.should.be(false);
                });
                optionEmpty.ifPresent((value) -> {
                    true.should.be(false);
                }).ifNotPresent(() -> {
                    true.should.be(true);
                    called++;
                });
                called.should.be(2);
            });
            it("should map Some's", {
                optionSet.map( val -> val * 2).should.equal(Some(10));
            });
        });
    }
}
