# Haxe Extension-Tools

Use `using <ToolsClass>` to get the required Function-Extensions

The Following extensions exists:
 - `extension.tools.OptionTools`
   - `create(Null<T>) -> Option<T>`
   - `Option<T>.ifPresent((T) -> Void) -> Option<T>`
   - `Option<T>.ifNotPresent(() -> Void) -> Option<T>`
   - `Option<T>.ifPresent() -> Bool`
   - `Option<T>.orDefault(defaultValue:T) -> T`
   - `Option<T>.orThrow(?String) -> T`

 - `extension.tools.DefaultTools`
   - `Null<T>.orDefault(defaultValue:T) -> T`
   - `Null<T>.orThrow(?String) -> T`
